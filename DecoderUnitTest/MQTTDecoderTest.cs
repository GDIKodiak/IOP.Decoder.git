﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using IOP.Protocols.MQTT.Package;
using IOP.Protocols.MQTT;
using System.Buffers;
using IOP.Decoder.MQTT;
using System.Threading.Tasks;
using System.IO;
using System.IO.Pipelines;
using System.Diagnostics;

namespace DecoderUnitTest
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class MQTTDecoderTest
    {
        private Random Random = new Random();

        [TestMethod]
        public void ConnentDecodeTest()
        {
            try
            {
                MQTTConnectOption option = new MQTTConnectOption();
                option.CleanSession = true;
                option.ClientIdentifier = "123512FSWGAW2";
                option.KeepAlive = 120;
                option.Password = Encoding.UTF8.GetBytes("TEST123456");
                option.PasswordFlag = true;
                option.ProtocolLevel = 0x40;
                option.UserName = "Test";
                option.UserNameFlag = true;
                option.WillFlag = true;
                option.WillMessage = Encoding.UTF8.GetBytes("Hello World");
                option.WillQoS = QoSType.QoS2;
                option.WillRetain = true;
                option.WillTopic = "/test";
                ConnectPackage package = new ConnectPackage(option);
                var result = package.ToBytes().ToArray();
                MemoryStream stream = new MemoryStream(result);
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var decodePackage = readPipe.Result;
                Assert.IsTrue(ConnectIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void ConnentDecodeTest2()
        {
            try
            {
                MQTTConnectOption option = new MQTTConnectOption();
                option.CleanSession = true;
                option.ClientIdentifier = "123512FSWGAW2";
                option.KeepAlive = 120;
                option.Password = Encoding.UTF8.GetBytes("TEST123456");
                option.PasswordFlag = true;
                option.ProtocolLevel = 0x40;
                option.UserName = "Test";
                option.UserNameFlag = true;
                option.WillFlag = true;
                option.WillMessage = Encoding.UTF8.GetBytes("Hello World");
                option.WillQoS = QoSType.QoS2;
                option.WillRetain = true;
                option.WillTopic = "/test";
                ConnectPackage package = new ConnectPackage(option);
                var result = package.ToBytes().ToArray();
                var decodePackage = MQTTDecoder.Decode(ref result);
                Assert.IsTrue(ConnectIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityConnentDecodeTest1()
        {
            try
            {
                MQTTConnectOption option = new MQTTConnectOption();
                option.CleanSession = true;
                option.ClientIdentifier = "123512FSWGAW2";
                option.KeepAlive = 120;
                option.Password = Encoding.UTF8.GetBytes("TEST123456");
                option.PasswordFlag = true;
                option.ProtocolLevel = 0x40;
                option.UserName = "Test";
                option.UserNameFlag = true;
                option.WillFlag = true;
                option.WillMessage = Encoding.UTF8.GetBytes("Hello World");
                option.WillQoS = QoSType.QoS2;
                option.WillRetain = true;
                option.WillTopic = "/test";
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    ConnectPackage package = new ConnectPackage(option);
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync2(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var results = readPipe.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityConnentDecodeTest2()
        {
            try
            {
                MQTTConnectOption option = new MQTTConnectOption();
                option.CleanSession = true;
                option.ClientIdentifier = "123512FSWGAW2";
                option.KeepAlive = 120;
                option.Password = Encoding.UTF8.GetBytes("TEST123456");
                option.PasswordFlag = true;
                option.ProtocolLevel = 0x40;
                option.UserName = "Test";
                option.UserNameFlag = true;
                option.WillFlag = true;
                option.WillMessage = Encoding.UTF8.GetBytes("Hello World");
                option.WillQoS = QoSType.QoS2;
                option.WillRetain = true;
                option.WillTopic = "/test";
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    ConnectPackage package = new ConnectPackage(option);
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                var read = ReadStream(stream);
                Task.WaitAll(read);
                var results = read.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void ConnackDecodeTest()
        {
            try
            {
                ConnackPackage package = new ConnackPackage(false, ReturnCodeType.Authorize);
                var result = package.ToBytes().ToArray();
                MemoryStream stream = new MemoryStream(result);
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var decodePackage = readPipe.Result;
                Assert.IsTrue(ConnackIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void ConnackDecodeTest2()
        {
            try
            {
                ConnackPackage package = new ConnackPackage(false, ReturnCodeType.Authorize);
                var result = package.ToBytes().ToArray();
                var decodePackage = MQTTDecoder.Decode(ref result);
                Assert.IsTrue(ConnackIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityConnackDecodeTest1()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    ConnackPackage package = new ConnackPackage(false, ReturnCodeType.Authorize);
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync2(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var results = readPipe.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityConnackDecodeTest2()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    ConnackPackage package = new ConnackPackage(false, ReturnCodeType.Authorize);
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                var read = ReadStream(stream);
                Task.WaitAll(read);
                var results = read.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void PublishDeocdeTest()
        {
            try
            {
                PublishPackage package = new PublishPackage("/test", 34412, Encoding.UTF8.GetBytes(text), 0);
                package.DUP = true;
                package.QoS = QoSType.QoS2;
                package.RETAIN = true;
                package.Body = Encoding.UTF8.GetBytes(text);
                var result = package.ToBytes().ToArray();
                MemoryStream stream = new MemoryStream(result);
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var decodePackage = readPipe.Result;
                Assert.IsTrue(PublishIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void PublishDeocdeTest2()
        {
            try
            {
                PublishPackage package = new PublishPackage("/test", 34412, Encoding.UTF8.GetBytes(text), 0);
                package.DUP = true;
                package.QoS = QoSType.QoS2;
                package.RETAIN = true;
                package.Body = Encoding.UTF8.GetBytes(text);
                var result = package.ToBytes().ToArray();
                var decodePackage = MQTTDecoder.Decode(ref result);
                Assert.IsTrue(PublishIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityPublishDecodeTest1()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    PublishPackage publish = new PublishPackage("/uav", (ushort)Random.Next(1, ushort.MaxValue), Encoding.UTF8.GetBytes($"X:{Random.Next()};Y:{Random.Next()};Z:{Random.Next()}"));
                    bytes.AddRange(publish.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync2(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var results = readPipe.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityPublishDecodeTest2()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    PublishPackage publish = new PublishPackage("/uav", (ushort)Random.Next(1, ushort.MaxValue), Encoding.UTF8.GetBytes($"X:{Random.Next()};Y:{Random.Next()};Z:{Random.Next()}"));
                    bytes.AddRange(publish.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                var read = ReadStream(stream);
                Task.WaitAll(read);
                var results = read.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void PubackDecodeTest()
        {
            try
            {
                PubackPackage package = new PubackPackage(34412);
                var result = package.ToBytes().ToArray();
                MemoryStream stream = new MemoryStream(result);
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var decodePackage = readPipe.Result;
                Assert.IsTrue(PubackIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void PubackDecodeTest2()
        {
            try
            {
                PubackPackage package = new PubackPackage(34412);
                var result = package.ToBytes().ToArray();
                var decodePackage = MQTTDecoder.Decode(ref result);
                Assert.IsTrue(PubackIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityPubackDecodeTest1()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    PubackPackage package = new PubackPackage((ushort)Random.Next(1, ushort.MaxValue));
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync2(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var results = readPipe.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityPubackDecodeTest2()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    PubackPackage package = new PubackPackage((ushort)Random.Next(1, ushort.MaxValue));
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                var read = ReadStream(stream);
                Task.WaitAll(read);
                var results = read.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void SubscribeDecodeTest()
        {
            try
            {
                var topics = new TopicFilter[]
                {
                    new TopicFilter("/test1",QoSType.QoS0),
                    new TopicFilter("/test2",QoSType.QoS1),
                    new TopicFilter("/test/test/test3",QoSType.QoS2)
                };
                SubscribePackage package = new SubscribePackage(34412, topics);
                var result = package.ToBytes().ToArray();
                MemoryStream stream = new MemoryStream(result);
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var decodePackage = readPipe.Result;
                Assert.IsTrue(SubscribeIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void SubscribeDecodeTest2()
        {
            try
            {
                var topics = new TopicFilter[]
                {
                    new TopicFilter("/test1",QoSType.QoS0),
                    new TopicFilter("/test2",QoSType.QoS1),
                    new TopicFilter("/test/test/test3",QoSType.QoS2)
                };
                SubscribePackage package = new SubscribePackage(34412, topics);
                var result = package.ToBytes().ToArray();
                var decodePackage = MQTTDecoder.Decode(ref result);
                Assert.IsTrue(SubscribeIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void SubscribeDecodeTest3()
        {
            SubscribePackage package = new SubscribePackage(34412, null);
            var result = package.ToBytes().ToArray();
            var decodePackage = MQTTDecoder.Decode(ref result);
            Assert.IsTrue(SubscribeIsSame(package, decodePackage));
            var result2 = package.ToBytes().ToArray();
            MemoryStream stream = new MemoryStream(result2);
            Pipe pipe = new Pipe();
            var fillPipe = FillPipeAsync(stream, pipe.Writer);
            var readPipe = ReadPipeAsync(pipe.Reader);
            Task.WaitAll(fillPipe, readPipe);
            var decodePackage2 = readPipe.Result;
            Assert.IsTrue(SubscribeIsSame(package, decodePackage2));
        }

        [TestMethod]
        public void QuantitySubscribeDecodeTestt1()
        {
            try
            {
                var topics = new TopicFilter[]
                {
                    new TopicFilter("/test1",QoSType.QoS0),
                    new TopicFilter("/test2",QoSType.QoS1),
                    new TopicFilter("/test/test/test3",QoSType.QoS2)
                };
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    SubscribePackage package = new SubscribePackage((ushort)Random.Next(1, ushort.MaxValue), topics);
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync2(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var results = readPipe.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantitySubscribeDecodeTestt2()
        {
            try
            {
                var topics = new TopicFilter[]
                {
                    new TopicFilter("/test1",QoSType.QoS0),
                    new TopicFilter("/test2",QoSType.QoS1),
                    new TopicFilter("/test/test/test3",QoSType.QoS2)
                };
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    SubscribePackage package = new SubscribePackage((ushort)Random.Next(1, ushort.MaxValue), topics);
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                var read = ReadStream(stream);
                Task.WaitAll(read);
                var results = read.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void SubackDecodeTest()
        {
            try
            {
                var subackCode = new SubackCode[] { SubackCode.SuccessQoS0, SubackCode.SuccessQoS1, SubackCode.SuccessQoS1, SubackCode.FailedQoS, SubackCode.SuccessQoS2 };
                SubackPackage package = new SubackPackage(34412, subackCode);
                var result = package.ToBytes().ToArray();
                MemoryStream stream = new MemoryStream(result);
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var decodePackage = readPipe.Result;
                Assert.IsTrue(SubackIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void SubackDecodeTest2()
        {
            try
            {
                var subackCode = new SubackCode[] { SubackCode.SuccessQoS0, SubackCode.SuccessQoS1, SubackCode.SuccessQoS1, SubackCode.FailedQoS, SubackCode.SuccessQoS2 };
                SubackPackage package = new SubackPackage(34412, subackCode);
                var result = package.ToBytes().ToArray();
                var decodePackage = MQTTDecoder.Decode(ref result);
                Assert.IsTrue(SubackIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantitySubackDecodeTest1()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    var subackCode = new SubackCode[] { SubackCode.SuccessQoS0, SubackCode.SuccessQoS1, SubackCode.SuccessQoS1, SubackCode.FailedQoS, SubackCode.SuccessQoS2 };
                    SubackPackage package = new SubackPackage((ushort)Random.Next(1, ushort.MaxValue), subackCode);
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync2(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var results = readPipe.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantitySubackDecodeTest2()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    var subackCode = new SubackCode[] { SubackCode.SuccessQoS0, SubackCode.SuccessQoS1, SubackCode.SuccessQoS1, SubackCode.FailedQoS, SubackCode.SuccessQoS2 };
                    SubackPackage package = new SubackPackage((ushort)Random.Next(1, ushort.MaxValue), subackCode);
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                var read = ReadStream(stream);
                Task.WaitAll(read);
                var results = read.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void UnsubscribeDecodeTest()
        {
            try
            {
                string[] topices = new string[] { "/test1", "/test2", "test45/test46/test46" };
                UnsubscribePackage package = new UnsubscribePackage(34412, topices);
                var result = package.ToBytes().ToArray();
                MemoryStream stream = new MemoryStream(result);
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var decodePackage = readPipe.Result;
                Assert.IsTrue(UnsubsrcibeIsSame(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void UnsubscribeDecodeTest2()
        {
            string[] topices = new string[] { "/test1", "/test2", "test45/test46/test46" };
            UnsubscribePackage package = new UnsubscribePackage(34412, topices);
            var result = package.ToBytes().ToArray();
            var doceodePackage = MQTTDecoder.Decode(ref result);
            Assert.IsTrue(UnsubsrcibeIsSame(package, doceodePackage));
        }
        [TestMethod]
        public void UnsubscribeDecodeTest3()
        {
            UnsubscribePackage package = new UnsubscribePackage(34412, null);
            var result = package.ToBytes().ToArray();
            var decodePackage = MQTTDecoder.Decode(ref result);
            Assert.IsTrue(UnsubsrcibeIsSame(package, decodePackage));
            var result2 = package.ToBytes().ToArray();
            MemoryStream stream = new MemoryStream(result2);
            Pipe pipe = new Pipe();
            var fillPipe = FillPipeAsync(stream, pipe.Writer);
            var readPipe = ReadPipeAsync(pipe.Reader);
            Task.WaitAll(fillPipe, readPipe);
            var decodePackage2 = readPipe.Result;
            Assert.IsTrue(UnsubsrcibeIsSame(package, decodePackage2));
        }
        [TestMethod]
        public void QuantityUnsubscribeDecodeTest1()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                string[] topices = new string[] { $"/test1", $"/test2", "test45/test46/test46" };
                for (int i = 0; i < 1000000; i++)
                {
                    UnsubscribePackage package = new UnsubscribePackage((ushort)Random.Next(1, ushort.MaxValue), topices);
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync2(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var results = readPipe.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityUnsubscribeDecodeTest2()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                string[] topices = new string[] { $"/test1", $"/test2", "test45/test46/test46" };
                for (int i = 0; i < 1000000; i++)
                {
                    UnsubscribePackage package = new UnsubscribePackage((ushort)Random.Next(1, ushort.MaxValue), topices);
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                var read = ReadStream(stream);
                Task.WaitAll(read);
                var results = read.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void UnSubackDecodeTest()
        {
            try
            {
                UnsubackPackage package = new UnsubackPackage(34412);
                var result = package.ToBytes().ToArray();
                MemoryStream stream = new MemoryStream(result);
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var decodePackage = readPipe.Result;
                Assert.IsTrue(UusubackIsSeam(package, decodePackage));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void UnSubackDecodeTest2()
        {
            try
            {
                UnsubackPackage package = new UnsubackPackage(34412);
                var result = package.ToBytes().ToArray();
                var decode = MQTTDecoder.Decode(ref result);
                Assert.IsTrue(UusubackIsSeam(package, decode));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityUnSubackDecodeTest1()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    UnsubackPackage package = new UnsubackPackage((ushort)Random.Next(1, ushort.MaxValue));
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync2(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var results = readPipe.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityUnSubackDecodeTest2()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    UnsubackPackage package = new UnsubackPackage((ushort)Random.Next(1, ushort.MaxValue));
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                var read = ReadStream(stream);
                Task.WaitAll(read);
                var results = read.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void QuantityPingreqDecodeTest1()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    PingreqPackage package = new PingreqPackage();
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync2(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var results = readPipe.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityPingreqDecodeTest2()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    PingreqPackage package = new PingreqPackage();
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                var read = ReadStream(stream);
                Task.WaitAll(read);
                var results = read.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void QuantityDisconnectDecodeTest1()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    DisconnectPackage package = new DisconnectPackage();
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                Pipe pipe = new Pipe();
                var fillPipe = FillPipeAsync(stream, pipe.Writer);
                var readPipe = ReadPipeAsync2(pipe.Reader);
                Task.WaitAll(fillPipe, readPipe);
                var results = readPipe.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        [TestMethod]
        public void QuantityDisconnectDecodeTest2()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                for (int i = 0; i < 1000000; i++)
                {
                    DisconnectPackage package = new DisconnectPackage();
                    bytes.AddRange(package.ToBytes().ToArray());
                }
                MemoryStream stream = new MemoryStream(bytes.ToArray());
                var read = ReadStream(stream);
                Task.WaitAll(read);
                var results = read.Result;
                Assert.AreEqual(1000000, results.Count);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        private Stopwatch stopwatch = new Stopwatch();
        /// <summary>
        /// 填充管道
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        private async Task FillPipeAsync(Stream steam, PipeWriter writer)
        {
            try
            {
                while (true)
                {
                    Memory<byte> memory = writer.GetMemory(1024);
                    int bytesRead = await steam.ReadAsync(memory);
                    writer.Advance(bytesRead);
                    if (bytesRead == 0) break;
                    FlushResult result = await writer.FlushAsync();
                    if (result.IsCompleted) break;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                writer.Complete();
            }
        }
        /// <summary>
        /// 读取缓冲区数据
        /// </summary>
        /// <param name="file"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        private async Task<IMQTTPackage> ReadPipeAsync(PipeReader reader)
        {
            try
            {
                IMQTTPackage package = null;
                while (true)
                {
                    ReadResult result = await reader.ReadAsync();
                    ReadOnlySequence<byte> buffer = result.Buffer;
                    stopwatch.Start();
                    package = MQTTDecoder.Decode(ref buffer);
                    stopwatch.Stop();
                    Debugger.Log(0, null, $"Time :{stopwatch.ElapsedMilliseconds}");

                    // We sliced the buffer until no more data could be processed
                    // Tell the PipeReader how much we consumed and how much we left to process
                    reader.AdvanceTo(buffer.End);
                    if (result.IsCompleted) break;
                }
                return package;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                reader.Complete();
            }
        }
        /// <summary>
        /// 读取缓冲区数据(版本2)
        /// </summary>
        /// <param name="file"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        private async Task<List<IMQTTPackage>> ReadPipeAsync2(PipeReader reader)
        {
            try
            {
                List<IMQTTPackage> packages = new List<IMQTTPackage>();
                while (true)
                {
                    ReadResult result = await reader.ReadAsync();
                    ReadOnlySequence<byte> buffer = result.Buffer;
                    while (!buffer.IsEmpty)
                    {
                        var package = MQTTDecoder.Decode(ref buffer);
                        if (package == null) break;
                        else packages.Add(package);
                    }
                    reader.AdvanceTo(buffer.Start, buffer.End);
                    if (result.IsCompleted) break;
                }
                return packages;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                reader.Complete();
            }
        }
        /// <summary>
        /// 使用老式数据流方式读取数据
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private Task<List<IMQTTPackage>> ReadStream(Stream stream)
        {
            BinaryReader reader = new BinaryReader(stream);
            List<IMQTTPackage> packages = new List<IMQTTPackage>();
            try
            {
                var bytesBuffered = 0;
                byte[] buffers = new byte[2048];
                int bytesRead;
                while (true)
                {
                    try
                    {
                        bytesRead = reader.Read(buffers, bytesBuffered, buffers.Length - bytesBuffered);
                        if (bytesRead == 0) break;
                        if (reader.BaseStream.Position == reader.BaseStream.Length)
                        {
                            Span<byte> w = buffers;
                            w = w.Slice(0, bytesRead + bytesBuffered);
                            buffers = w.ToArray();
                        }
                        while (buffers.Length >= 0)
                        {
                            var package = MQTTDecoder.Decode(ref buffers);
                            if (package != null) packages.Add(package);
                            else if(package == null)
                            {
                                if(buffers.Length != 0)
                                {
                                    var newBuffer = new byte[2048];
                                    bytesBuffered = buffers.Length;
                                    Buffer.BlockCopy(buffers, 0, newBuffer, 0, bytesBuffered);
                                    buffers = newBuffer;
                                }
                                else
                                {
                                    bytesBuffered = 0;
                                    buffers = new byte[2048];
                                }
                                break;
                            }

                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
            catch (EndOfStreamException)
            {
                reader.Close();
            }
            finally
            {
                reader.Dispose();
            }
            return Task.FromResult(packages);
        }

        private bool ConnectIsSame(IMQTTPackage first, IMQTTPackage second)
        {
            bool result = true;
            ConnectPackage connect1 = (ConnectPackage)first;
            ConnectPackage connect2 = (ConnectPackage)second;

            if (connect1.CleanSession != connect2.CleanSession) result = false;
            if (connect2.ClientIdentifier != connect1.ClientIdentifier) result = false;
            if (connect2.KeepAlive != connect1.KeepAlive) result = false;
            if (connect1.PacketType != connect2.PacketType) result = false;
            if (connect2.Password.Length != connect1.Password.Length) result = false;
            if (connect1.PasswordFlag != connect2.PasswordFlag) result = false;
            if (connect1.ProtocolLevel != connect2.ProtocolLevel) result = false;
            if (connect1.ProtocolName != connect2.ProtocolName) result = false;
            if (connect1.RemainingLength != connect2.RemainingLength) result = false;
            if (connect1.UserName != connect2.UserName) result = false;
            if (connect1.UserNameFlag != connect2.UserNameFlag) result = false;
            if (connect1.WillFlag != connect2.WillFlag) result = false;
            if (connect1.WillMessage.Length != connect2.WillMessage.Length) result = false;
            if (connect1.WillQoS != connect2.WillQoS) result = false;
            if (connect1.WillRetain != connect2.WillRetain) result = false;
            if (connect1.WillTopic != connect2.WillTopic) result = false;
            return result;
        }
        private bool ConnackIsSame(IMQTTPackage first, IMQTTPackage second)
        {
            bool result = true;
            ConnackPackage connack1 = (ConnackPackage)first;
            ConnackPackage connack2 = (ConnackPackage)second;

            if (connack1.ConnectReturnCode != connack2.ConnectReturnCode) result = false;
            if (connack1.SessionPresent != connack2.SessionPresent) result = false;

            return result;
        }
        private bool PublishIsSame(IMQTTPackage first, IMQTTPackage second)
        {
            bool result = true;
            PublishPackage publish1 = (PublishPackage)first;
            PublishPackage publish2 = (PublishPackage)second;

            if (publish1.PacketType != publish2.PacketType) result = false;
            if (publish1.DUP != publish2.DUP) result = false;
            if (publish1.PacketIdentifier != publish2.PacketIdentifier) result = false;
            if (publish1.QoS != publish2.QoS) result = false;
            if (publish1.RemainingLength != publish2.RemainingLength) result = false;
            if (publish1.RETAIN != publish2.RETAIN) result = false;
            if (publish1.TopicName != publish2.TopicName) result = false;
            string data = Encoding.UTF8.GetString(publish2.Body);
            string data2 = Encoding.UTF8.GetString(publish1.Body);
            if (data != data2) result = false;
            return result;
        }
        private bool PubackIsSame(IMQTTPackage first, IMQTTPackage second)
        {
            var result = true;
            PubackPackage puback1 = (PubackPackage)first;
            PubackPackage puback2 = (PubackPackage)second;
            if (puback1.PacketIdentifier != puback2.PacketIdentifier) result = false;
            return result;
        }
        private bool PubcompIsSame(IMQTTPackage first, IMQTTPackage second)
        {
            var result = true;
            PubcompPackage puback1 = (PubcompPackage)first;
            PubcompPackage puback2 = (PubcompPackage)second;
            if (puback1.PacketIdentifier != puback2.PacketIdentifier) result = false;
            return result;
        }
        private bool SubscribeIsSame(IMQTTPackage first, IMQTTPackage second)
        {
            var result = true;
            SubscribePackage subscribe1 = (SubscribePackage)first;
            SubscribePackage subscribe2 = (SubscribePackage)second;

            if (subscribe1.PacketIdentifier != subscribe2.PacketIdentifier) result = false;
            if (subscribe1.RemainingLength != subscribe2.RemainingLength) result = false;
            if (subscribe1.TopicFilters.Length != subscribe2.TopicFilters.Length) result = false;
            return result;
        }
        private bool SubackIsSame(IMQTTPackage first, IMQTTPackage second)
        {
            var result = true;
            SubackPackage suback1 = (SubackPackage)first;
            SubackPackage suback2 = (SubackPackage)second;

            if (suback1.PacketIdentifier != suback2.PacketIdentifier) result = false;
            if (suback1.RemainingLength != suback2.RemainingLength) result = false;
            if (suback1.SubackCodes.Length != suback2.SubackCodes.Length) result = false;

            return result;
        }
        private bool UnsubsrcibeIsSame(IMQTTPackage first, IMQTTPackage second)
        {
            var result = true;

            UnsubscribePackage unsubscribe1 = (UnsubscribePackage)first;
            UnsubscribePackage unsubscribe2 = (UnsubscribePackage)second;

            if (unsubscribe1.PacketIdentifier != unsubscribe2.PacketIdentifier) result = false;
            if (unsubscribe1.TopicFilters.Length != unsubscribe2.TopicFilters.Length) result = false;

            return result;
        }
        private bool UusubackIsSeam(IMQTTPackage first, IMQTTPackage second)
        {
            var result = true;
            UnsubackPackage unsuback1 = (UnsubackPackage)first;
            UnsubackPackage unsuback2 = (UnsubackPackage)second;

            if (unsuback1.PacketIdentifier != unsuback2.PacketIdentifier) result = false;
            return result;
        }

        #region text
        private const string text = @"C# 指南提供了许多有关 C# 语言的资源。 此网站面向许多不同的受众群体。 你可能希望探索本指南的不同部分，具体视你的编程经验或 C# 语言和 .NET 使用经验而定。
对于从未接触过编程的开发者：
从浏览 C# 教程简介开始。 通过了解这些教程，可以在浏览器中交互式浏览 C# 语言。 可以从这里转到其他教程。 这些教程介绍了如何从头开始创建 C# 程序。 其中分步介绍了如何创建程序。 同时还解释了语言概念以及如何自行生成 C# 程序。 如果希望先阅读概述内容，请尝试阅读 C# 语言介绍。 其中介绍了 C# 语言的概念。 阅读完这一部分后，你将会对 C# 语言有一个基本的了解，可以尝试阅读各个教程或自行生成一些程序。
对于刚开始接触 C# 的开发者：
如果以前从事过开发工作，但是刚开始接触 C#，请阅读 C# 语言介绍。 其中涵盖了该语言的基本语法和结构，你可以通过“语言介绍部分”将 C# 与你用过的其他语言进行比较。 还可以浏览教程，尝试生成基本的 C# 程序。
对于 C# 经验丰富的开发者：
如果之前用过 C#，应先阅读此语言最新版本中新增的功能。 请参阅 C# 中的新增功能，了解当前版本中的新功能。
C# 指南分为多个部分。 可以按顺序阅读，也可以直接跳到最感兴趣的部分。 一些部分主要侧重于 C# 语言方面。 另一些部分介绍了端到端方案，展示了可以使用 C# 和 .NET Framework 创建的几种类型程序。
入门
此部分介绍了在首选平台上创建 C# 开发环境需要安装的程序。 此部分下的各个主题介绍了如何在不同的受支持环境中创建首个 C# 程序。
C# 教程简介：
C# 教程简介是为新手开发人员提供的互动教程，可让他们使用读取–求值–打印循环 (REPL) 接口在浏览器中探索和学习 C# 语言。 完成互动课程后，可以通过在自己的计算机上练习相同的课程来提高你的编码技能。
教程
此部分介绍了各种端到端方案，其中包括说明和代码。 其中演示了为什么首选特定的惯用做法、最适用于不同方案的 C# 功能，以及常见任务的参考实现。 如果最佳学习方式是查看代码，请先阅读此部分。 还可以下载所有代码，然后在你自己的环境中进行尝试。
C# 教程
此部分概述了 C# 语言。 其中介绍了 C# 程序的构成元素以及此语言的各项功能。 同时演示了所有 C# 语法元素的小示例，并讨论了主要的 C# 语言主题。
C# 中的新增功能
概述了在最新语言版本中添加的新功能以及 C# 语言的历史。
C# 编程指南
提供有关如何使用 C# 语言结构的信息和实例。
C# 语法高度重视表达，但学习起来也很简单轻松。 任何熟悉 C、C++ 或 Java 的人都可以立即认出 C# 的大括号语法。 通常情况下，了解上述任何一种语言的开发者可以在很短的时间内就开始使用 C# 高效工作。 C# 语法简化了 C++ 的许多复杂操作，并提供强大功能，如可以为 null 的值类型、枚举、委托、lambda 表达式和直接内存访问，而 Java 并不提供这些功能。 C# 不仅支持泛型方法和类型，提升了类型安全性和性能，还支持迭代器，以便集合类的实现者可以定义方便客户端代码使用的自定义迭代行为。 语言集成查询 (LINQ) 表达式让强类型查询成为最高级的语言构造。
作为面向对象的语言，C# 支持封装、继承和多态性这些概念。 所有变量和方法（包括作为应用程序入口点的 Main 方法）都封装在类定义中。 虽然类可能会直接继承一个父类，但可以实现任意数量的接口。 若要用方法重写父类中的虚方法，必须使用 override 关键字，以免发生意外重定义。 在 C# 中，结构就像是轻量级类，是可以实现接口但不支持继承的堆栈分配类型。
除了这些面向对象的基本原则，使用 C# 还可以通过以下多个创新语言构造更加轻松地开发软件组件：
封装的方法签名（名为“委托”），可实现类型安全事件通知。
用作私有成员变量的访问器的属性。
在运行时提供有关类型的声明性元数据的特性。
内联的 XML 文档注释。
语言集成查询 (LINQ)，提供跨各种数据源的内置查询功能。
如果需要与其他 Windows 软件（如 COM 对象或本机 Win32 DLL）进行交互，可以在 C# 中通过名为“互操作”的过程来实现。 借助互操作，C# 程序可以执行本机 C++ 应用程序可以执行的几乎任何操作。 在直接内存访问非常关键的情况下，C# 甚至支持指针和“不安全”代码的概念。";
        #endregion
    }


}
